let express = require('express');
let app = express();
let nodeMailer = require('nodemailer');
let bodyParser = require('body-parser');

app.set('view engine', 'ejs');

app.use('/static', express.static(__dirname + '/assets'));
app.use('/static/css', express.static(__dirname + '/node_modules/bulma/css'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.render('index');
});

app.post('/send-email', function (req, res) {
    if(req.body.name == '' || req.body.email == '' || req.body.subject == '' || req.body.body == ''){
        res.json({ 'success': false, 'error': 0})
    } else {
        let transporter = nodeMailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'benson.developper@gmail.com',
                pass: 'eqcrqwznhxhjfqfe'
            }
        });
        let mailOptions = {
            from: '"'+req.body.name+'" <'+req.body.email+'>', // sender address
            to: 'benson.developper@gmail.com', // list of receivers
            subject: req.body.subject, // Subject line
            text: req.body.body+'  [email:'+req.body.email+']', // plain text body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                res.json({ 'success': false, 'error': 1});
            }
            res.json({ 'success': true});
        });
    }

});

app.listen(8080);
console.log('8080 is the magic port');
